/*
 *
 */
const express = require('express');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));
hbs.registerPartials(__dirname + '/views/parciales');

app.set('view engine', 'hbs');


app.get('/', function(req, res) {
    //res.send('Hola Mundo')

    // res.render('home', {
    //     nombre: 'Rogelio',
    //     annio: new Date().getFullYear()
    // });

    res.render(__dirname + '/views/home', { nombre: 'Fernando' });
})

app.get('/about', function(req, res) {

    res.render(__dirname + '/views/about', { nombre: 'Fernando' });
})

app.listen(port, () => {
    console.log(`Escuchando en el puerto ${ port }`);
});