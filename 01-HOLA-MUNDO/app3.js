//crea el main al inicio del programa 


//se ejecuta al inicio del programa
console.log('Inicio del programa');


//node coloca em Node Apis la funcion para esperar la ejecucion
setTimeout(function() {
    console.log('Primer Timeout');
}, 3000);


//node coloca em Node Apis la funcion para esperar la ejecucion
//despues la manda a cola de callback para que sea ejecutada
setTimeout(function() {
    console.log('Segundo Timeout');
}, 0);


//node coloca em Node Apis la funcion para esperar la ejecucion
setTimeout(function() {
    console.log('Tercer Timeout');
}, 0);


//se ejecuta al inicio del programa
console.log('Fin del programa');