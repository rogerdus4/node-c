/*
 *
 */

// REQUIRES 
/*  Existen 3 tipos 
 * const fs = require('fs');  pertenece a node
 * const fs = require('express'); pertenece a un autor 
 * const fs = require('./fs'); creados por nosotros
 */

const argv = require('./config/yargs').argv;
const colors = require('colors');

const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar')

let comando = argv._[0];

switch (comando) {

    case 'listar':
        listarTabla(argv.base, argv.limite);
        //.then(archivo => console.log(`Tabla del ${ argv.base }`))
        //.catch(e => console.log(e))
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado`, colors.green(`${ archivo } del ${argv.base} al ${argv.limite}`)))
            .catch(e => console.log(e))
        break;
    default:
        console.log('Comando no reconocido');

}


/* Codigo de antes */
// let parametro = arvg[2];
// let base = parametro.split('=')[1]


//console.log('base', argv.base);