const fs = require('fs');
const colors = require('colors');


let listarTabla = (base, limite = 10) => {

    console.log('==========================');
    console.log(`Tabla de ${ base }`.green);
    console.log('==========================');

    for (let x = 1; x <= limite; x++) {

        console.log(`${ base } * ${ x } = ${ x * base}\n`);

    }
}


let crearArchivo = ((base, limite = 10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject('El valor introducido no es un numero');
            return;
        }

        let data = '';

        for (let x = 1; x <= limite; x++) {

            data += `${ base } * ${ x } = ${ x * base}\n`;

        }



        fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {

            if (err)
                reject(err)
            else
                resolve(`El archivo tabla ${ base }.txt`);

        });
    })

})

module.exports = {
    crearArchivo,
    listarTabla
}