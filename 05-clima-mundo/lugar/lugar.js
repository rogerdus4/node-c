const axios = require('axios');

const getLugarLatLng = async(direccion) => {

    let encodeUrl = encodeURI(direccion);

    let resp = await axios.get(`https://api.opencagedata.com/geocode/v1/json?q=${encodeUrl}&key=f3776e225cf34a2b8f078e43b0ae0562&language=es&pretty=1`)

    if (resp.data.total_results === 0) {
        throw new Error(`No hay resultados para la Ciudad ${ direccion } `);
    }

    let location = resp.data.results[0];
    let coors = location.geometry;

    return {
        direccion: location.formatted,
        latitud: coors.lat,
        longitud: coors.lat
    }

}

module.exports = {
    getLugarLatLng
}