/*
 *
 */
// const opts = {
//     descripcion: {
//         demand: true,
//         alias: 'd',
//         desc: 'DEscripcion de una tarea por hacer'

//     },
//     completado: {
//         default: true,
//         alias: 'c'

//     }
// }

const descripcion = {

    demand: true,
    alias: 'd',
    desc: 'Descripcion de una tarea por hacer'
};

const completado = {
    default: true,
    alias: 'c'

}

const argv = require('yargs')
    .command('crear', 'Crear un elemento por hacer', {
        descripcion
    })
    .command('actualizar', 'Actualiza el estado completado de una tarea', {

        descripcion,
        completado
    })
    .command('borrar', 'Borra unatarea', {

        descripcion
    })
    .help()
    .argv;


module.exports = {
    argv
}